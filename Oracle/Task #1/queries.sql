/*1. Display all information from the Countries table.*/

SELECT *
FROM countries

/*2. Display all information from the Jobs table*/

SELECT *
FROM jobs

/*3. List the departments and their names that are present in the Employees table.
The rows in the final sample should not be repeated.*/

SELECT DISTINCT department_name
FROM departments

/*4. Choose the names of employees, start date, salary. The final sample should look like:
first_name_last_name hire_date salary*/

SELECT first_name || '_' || last_name, hire_date, salary
FROM employees

/*5. Check: Are there fields with empty values in the Employees table.
Print any of the columns that had such values, pre-replacing "empty" values with "non-empty".
Set the default value for the "empty" field yourself.*/

SELECT NVL(commission_pct, 300)
FROM employees
WHERE commission_pct IS NULL

/*6. Choose the names of employees, indicate the annual salary of each, position, name of the manager*/

SELECT first_name || '_' || last_name, salary*12,job_title, manager_id 
FROM employees
INNER JOIN jobs ON
employees.job_id = jobs.job_id

/*7. Select employees who do not receive the award*/

SELECT first_name || '_' || last_name
FROM employees
WHERE commission_pct = 0

/*8. Indicate the amount of semi-annual income of each employee, given that the bonus is specified as a percentage.*/

SELECT first_name || '_' || last_name, (salary*6)+salary*NVL(commission_pct, 0)*6 AS 'halfyear_salary'
FROM employees

/*9. Search for employees whose salary is less than the maximum possible and more than the minimum possible salary.
Print the employee's last name, salary*/

SELECT last_name, salary
FROM employees
INNER JOIN jobs ON
employees.job_id = jobs.job_id
WHERE salary > min_salary AND salary < max_salary

/*10. Put in order the decline of countries in which there are branches of the company*/

SELECT DISTINCT country_name
FROM 
(departments INNER JOIN locations ON
departments.location_id = locations.location_id)
INNER JOIN countries ON
locations.country_id = countries.country_id
ORDER BY country_name DESK

/*11. Print the names of employees whose names end in the letters "K", "L", "M", "N", "O", "P"*/

SELECT first_name
FROM employees
WHERE last_name LIKE '%[k-p]'

/*12. Print the names of employees with the second letter "a" in their last names*/

SELECT first_name
FROM employees
WHERE last_name LIKE '_a%'

/*13. Write a query that will identify employees who do not have subordinates.*/

SELECT first_name || '_' || last_name
FROM employees
WHERE manager_id IS NOT NULL

/*14. Make a request that forms a source document that will indicate the employee's net income (net of taxes) for the year. 
It is known that all employee income is taxed. Tax rates should be conditionally accepted as: 3% - pension fund,
5% - social insurance fund, 20% - income tax. The bonus is also included in the employee's income.*/

SELECT first_name || '_' || last_name, ((salary * 12) * 0.72 + NVL(comission_pct, 0)) AS "year_salary"
FROM employees

/*15. Print the last name, first name, position, name of the department for those employees
whose salary is between the two values you specify.*/

SELECT first_name || '_' || last_name, job_title, department_name
FROM 
(employees INNER JOIN jobs ON
employees.job_id = jobs.job_id)
INNER JOIN departments ON
employees.department_id = departments.department_id
WHERE salary > 6000 AND salary < 19000